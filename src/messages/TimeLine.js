import React, { Component } from 'react';
import PropTypes from 'prop-types';

class TimeLine extends Component {
    render() {
        const { date: dateStr } = this.props;
        const date = new Date(dateStr).toDateString();
        return (
            <div>
                <div className="flex-center">
                    <div>{date}</div>
                </div>
                <hr />

            </div>
        );
    }
}

TimeLine.propTypes = {
    date: PropTypes.string.isRequired
}

export default TimeLine;