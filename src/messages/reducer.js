import { ADD_MESSAGE, UPDATE_MESSAGE, DELETE_MESSAGE, SET_MESSAGES, SET_LIKE } from './actionTypes';

const initialState = [];

export default function (state = initialState, action) {
    switch (action.type) {
        case ADD_MESSAGE: {
            const { id, data } = action.payload;
            const newMessage = { id, ...data };
            return [...state, newMessage];
        }

        case UPDATE_MESSAGE: {
            const { id, data } = action.payload;
            const updatedMessages = state.map(message => {
                if (message.id === id) {
                    return {
                        ...message,
                        ...data
                    };
                } else {
                    return message;
                }
            })
            return updatedMessages;
        }

        case DELETE_MESSAGE: {
            const { id } = action.payload;
            const filteredMessages = state.filter(message => message.id !== id);
            return filteredMessages;
        }

        case SET_MESSAGES: {
            const { messages } = action.payload;
            return messages;
        }

        case SET_LIKE: {
            const { id, userId } = action.payload;
            const message = state.find(message => message.id === id);
            const reaction = message.reactions.find(react => react.userId === userId);

            const updatedMessage = { ...message };
            if (!reaction) {
                updatedMessage.reactions.push({ userId });
            } else {
                updatedMessage.reactions = message.reactions
                    .filter(reaction => reaction.userId !== userId);
            }
            const updatedMessages = state.map(msg => {
                if (msg.id === id) {
                    return updatedMessage;
                } else {
                    return msg;
                }
            })
            return updatedMessages;
        }

        default:
            return state;
    }
}